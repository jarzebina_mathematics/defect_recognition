Application for testing pattern recognition theory in defect recognition problems.

## Using
The "settings.json" file must be located in the same directory as the executable file.

===========

## Report
The report (doc/report) is based on template: https://github.com/latex-g7-32/latex-g7-32.
See template readme in doc/report. (pdflatex work). 

## Авторы 
Дмитрий Коротаев (flyvolantrider@gmail.com)


Станислав Ряузов
