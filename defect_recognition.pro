TARGET = defect_recognition
TEMPLATE = app

#QT += core
QT -= gui

CONFIG += c++17 console qt object_parallel_to_source warn_on
CONFIG -= app_bundle

linux-g++: QMAKE_CXXFLAGS += -std=c++17

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG(debug, debug|release) {
    _BUILD_TYPE = debug
} else {
    _BUILD_TYPE = release
}

_PROJECT_ROOT = $${PWD}
_BIN = $${_PROJECT_ROOT}/bin/$${_BUILD_TYPE}
_BUILD = $${_PROJECT_ROOT}/build/$${_BUILD_TYPE}
_SRC = $${_PROJECT_ROOT}/src

HEADERS += \
    $${_SRC}/classifier/classifier.h \
    $${_SRC}/direct_problem/direct_problem.h \
    $${_SRC}/direct_problem_solver/direct_problem_solver.h \
    $${_SRC}/r_boundary_temp/r_boundary_temp.h \
    $${_SRC}/defect/defect.h \
    $${_SRC}/generator/generator.h \
    $${_SRC}/measurer/measurer.h \
    $${_SRC}/heater/contact_heater.h \
    $${_SRC}/heater/heating_flow.h \
    $${_SRC}/heater/i_heater.h \
    $${_SRC}/rod/rod.h

SOURCES += \
        $${_SRC}/classifier/classifier.cpp \
        $${_SRC}/direct_problem/direct_problem.cpp \
        $${_SRC}/direct_problem_solver/direct_problem_solver.cpp \
        $${_SRC}/r_boundary_temp/r_boundary_temp.cpp \
        $${_SRC}/main.cpp \
        $${_SRC}/defect/defect.cpp \
        $${_SRC}/generator/generator.cpp \
        $${_SRC}/measurer/measurer.cpp \
        $${_SRC}/heater/contact_heater.cpp \
        $${_SRC}/heater/heating_flow.cpp \
        $${_SRC}/rod/rod.cpp

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

MOC_DIR = $${_BUILD}/moc/
OBJECTS_DIR = $${_BUILD}/obj/
DESTDIR = $${_BIN}/
VERSION = 0.1.0    # major.minor.patch
