// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include "direct_problem_solver.h"

#include <QFile>
#include <QJsonObject>

#include "../direct_problem/direct_problem.h"
#include "../r_boundary_temp/r_boundary_temp.h"
#include "../direct_problem/direct_problem.h"
#include "../rod/rod.h"
#include "../heater/i_heater.h"

constexpr static const char* JSON_TAU = "tau";
constexpr static const char* JSON_H = "h";
constexpr static const char* JSON_T_THIN = "tThinning";
constexpr static const char* JSON_X_THIN = "xThinning";

DirectProblemSolver::DirectProblemSolver()
{
    initConsts();
}

bool DirectProblemSolver::operator ()(const DirectProblem &problem, QString file) const
{
    const quint64 N_T = problem.time() / tau_ + 1; //КОЛИЧЕСТОВ УЗЛОВ (последний индекс N_T - 1)
    const quint64 N_X = problem.rod().l() / h_ + 1;

    constexpr char SEPARATOR = ';';
    QFile f(file);
    f.remove();
    if (!f.open(QFile::Text | QFile::WriteOnly))
    {
        return false;
    }
    QTextStream stream(&f);

    auto saver = [this, N_T, N_X, &stream](quint64 iT, const QVector<double>& u)
    {
        if (iT % tThinning_ == 0 || iT == N_T - 1)
        {
            stream << iT * tau_ << SEPARATOR;
            quint64 iX;
            for (iX = 0; iX < N_X - 1; iX += xThinning_)
            {
                stream << u[iX] << SEPARATOR;
            }
            stream << u[N_X - 1] << SEPARATOR;
            stream << endl;
        }
    };

    solve(problem, saver);
    return true;
}

RBoundaryTemp DirectProblemSolver::operator ()(const DirectProblem &problem) const
{
    const quint64 N_T = problem.time() / tau_ + 1; //КОЛИЧЕСТОВ УЗЛОВ (последний индекс N_T - 1)
    const quint64 N_X = problem.rod().l() / h_ + 1;

    RBoundaryTemp temp;
    temp.reserve(N_T / tThinning_ + 2);

    auto saver = [this, N_T, N_X, &temp](quint64 iT, const QVector<double>& u)
    {
        if (iT % tThinning_ == 0 || iT == N_T - 1)
        {
            temp.push_back(u[N_X - 1]);
        }
    };

    solve(problem, saver);

    return temp;
}

bool DirectProblemSolver::init(const QJsonObject &obj)
{
    {
        auto v = obj[JSON_TAU];
        if (!v.isDouble())
        {
            return false;
        }
        setTau(v.toDouble());
    }
    {
        auto v = obj[JSON_H];
        if (!v.isDouble())
        {
            return false;
        }
        setH(v.toDouble());
    }
    {
        auto v = obj[JSON_T_THIN];
        if (!v.isDouble())
        {
            return false;
        }
        setTThinning(v.toInt());
    }
    {
        auto v = obj[JSON_X_THIN];
        if (!v.isDouble())
        {
            return false;
        }
        setXThinning(v.toInt());
    }
    return true;
}

double DirectProblemSolver::tau() const
{
    return tau_;
}

void DirectProblemSolver::setTau(double tau)
{
    tau_ = tau;
    initConsts();
}

double DirectProblemSolver::h() const
{
    return h_;
}

void DirectProblemSolver::setH(double h)
{
    h_ = h;
    initConsts();
}

quint64 DirectProblemSolver::tThinning() const
{
    return tThinning_;
}

void DirectProblemSolver::setTThinning(const quint64 &tThinning)
{
    tThinning_ = tThinning;
}

quint64 DirectProblemSolver::xThinning() const
{
    return xThinning_;
}

void DirectProblemSolver::setXThinning(const quint64 &xThinning)
{
    xThinning_ = xThinning;
}

void DirectProblemSolver::solve(const DirectProblem &problem, const SaveFnc &saver) const
{
    const auto& rod = problem.rod();
    const quint64 N_T = problem.time() / tau_ + 1; //КОЛИЧЕСТОВ УЗЛОВ (последний индекс N_T - 1)
    const quint64 N_X = rod.l() / h_ + 1;

    QVector<double> u(N_X, problem.initTemp());
    QVector<double> uPrev(N_X, problem.initTemp());

    std::function<void(double)> calcLBound;
    //Инициализация функтора аппроксимирующее ГУ нагревателя
    switch (problem.heater().boundaryConditionType())
    {
    case 1:
    {
        auto& heater = problem.heater();
        //        double& lBound = uNext[0];
        calcLBound = [&heater, &u](double t)
        {
            u[0] = heater(t);
        };
        break;
    }
    case 2:
    {
        const double H_LAMDA = h_ / rod.lambda();
        auto& heater = problem.heater();
        calcLBound = [&heater, &u, H_LAMDA](double t)
        {
            u[0] = u[1] + H_LAMDA * heater(t);
        };
        break;
    }
    default:
    {
        Q_ASSERT(true);
    }
    }
    for (quint64 iT = 1; iT < N_T; ++iT)
    {
        for (quint64 iX = 1; iX < N_X - 1; ++iX)
        {
            double x = iX * h_;
            double aB = problem.a(x - h_2_);
            double aF = problem.a(x + h_2_);
            u[iX] = uPrev[iX] + tau_h2_ *
                    (aF * uPrev[iX + 1] + aB *  uPrev[iX - 1] - (aB + aF) * uPrev[iX]);
        }
        calcLBound(iT * tau_);
        u[N_X - 1] = u[N_X - 2];
        saver(iT, u);
        u.swap(uPrev);
    }
}

void DirectProblemSolver::initConsts()
{
    tau_h2_ = tau_ / h_ / h_;
    h_2_  = h_ / 2.;
}
