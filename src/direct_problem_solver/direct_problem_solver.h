// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DIRECTPROBLEMSOLVER_H
#define DIRECTPROBLEMSOLVER_H

#include <functional>

#include <QString>
#include <QVector>

/**
 * @brief Прямая задача теплопроводности одномерного стежня  с дефектом
 */
class DirectProblem;

/**
 * @brief Табличная функция температуры на правой границе во времени
 */
class RBoundaryTemp;

class QJsonObject;

/**
 * @brief Решатель прямой задачи теплопроводности одномерного стежня  с дефектом
 * Явная разностная схема для уравнения с переменным коэф.температурапроводности.
 */
class DirectProblemSolver
{
public:
    explicit DirectProblemSolver();
    /**
     * @brief Решает задачу и записывает результат в файл
     * @param problem прямая задача
     * @param file имя файла
     * Формат файла csv
     */
    bool operator ()(const DirectProblem& problem, QString file) const;
    /**
     * @brief Решает задачу и возвращает таблицу-функцию температуры на правой границе во времени
     * @param problem прямая задача
     * @return возвращает таблицу-функцию температуры на правой границе во времени
     */
    RBoundaryTemp operator ()(const DirectProblem& problem) const;

    bool init(const QJsonObject &obj);

    double tau() const;
    void setTau(double tau);

    double h() const;
    void setH(double h);

    quint64 tThinning() const;
    void setTThinning(const quint64 &tThinning);

    quint64 xThinning() const;
    void setXThinning(const quint64 &xThinning);

private:
    using SaveFnc = std::function<void (quint64, const QVector<double>&)> ;
    void solve(const DirectProblem &problem, const SaveFnc& saver) const;

    double tau_ = 1E-3; //шаг по времени
    double h_ = 1E-2; //шаг по координате
    quint64 tThinning_ = 400000; //коэфициент прореживания по времени
    quint64 xThinning_ = 4; //коэфициент прореживания по координате

    void initConsts();

    double tau_h2_;
    double h_2_;

};

#endif // DIRECTPROBLEMSOLVER_H
