// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <optional>

#include <QString>

#include "../defect/defect.h"

/**
 * @brief Измеритель растояний
 */
class Measurer;

/**
 * @brief Табличная функция температуры на правой границе во времени
 */
class RBoundaryTemp;

class QJsonObject;

/**
 * @brief Классификатор
 * Ближайший класс определяется через растояния до центров классов.
 */
class Classifier
{
public:
    /**
     * @brief Классифицирует выборку из заданного csv файла и сохраняет результат в JSON файл
     * @param defectFile csv файл выборки дефектов
     * @param tempFile csv файл температур на правой границе соответсвующий стержней выборки
     * @param outputFile JSON файл классов
     * @param measurer измеритель
     */
    bool operator ()(QString defectFile, QString tempFile, QString outputFile,
                    const Measurer &measurer);
    /**
     * @brief Определяет какому классу из JSON файла соответстуюет таблица температур
     * @param temp Табличная функция температуры
     * @param file JSON файл классов
     * @return Опционально номер класса
     * Если нарушен формат файла, то возвращается nullopt
     */
    std::optional<std::pair<quint64, Defect>> operator()(const RBoundaryTemp& temp, QString file);

    bool init(const QJsonObject &obj);

    quint64 nClasses() const;
    void setNClasses(const quint64 &nClasses);

private:
    quint64 nClasses_ = 70;

};

#endif // CLASSIFIER_H
