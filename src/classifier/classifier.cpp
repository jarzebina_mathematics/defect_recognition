// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include "classifier.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include "../r_boundary_temp/r_boundary_temp.h"
#include "../measurer/measurer.h"

constexpr static const char* JSON_CLASSES = "classes";
constexpr static const char* JSON_CENTRE_DEFECT = "centre_defect";
constexpr static const char* JSON_MEDIUM_BOUNDARY_TEMP = "mediumBoundaryTemp";
constexpr static const char* JSON_MEMBERS = "members";
constexpr static const char* JSON_DEFECT = "defect";
constexpr static const char* JSON_BOUNDARY_TEMP = "boundaryTemp";

constexpr static const char* JSON_N_CLASSES = "nClasses";

constexpr double EPS = 1E-8;

bool Classifier::operator ()(QString defectFile, QString tempFile, QString outputFile,
                             const Measurer &measurer)
{
    QVector<Defect> defects;
    {
        QFile defectF(defectFile);
        if (!defectF.open(QFile::Text | QFile::ReadOnly))
        {
            return false;
        }
        QTextStream defectStream(&defectF);
        while (!defectStream.atEnd())
        {
            Defect d;
            defectStream >> d;
            defects.append(d);
        }
        if (!defects.size())
        {
            return false;
        }
    }
    QVector<quint64> defectClass(defects.size());
    QVector<Defect> centers(nClasses_);
    for(quint64 iClass = 0; iClass < nClasses_; ++iClass)
    {
        centers[iClass] = defects[iClass];
    }
    QVector<quint64> classSize(nClasses_);
    {
        QVector<Defect> newCenters(nClasses_, Defect{0,0,0});
        bool isEnd = false;
        while (!isEnd)
        {
            classSize.fill(0);
            for(quint64 iDefect = 0; iDefect < defects.size(); ++iDefect)
            {
                quint64 iNearestClass = 0;
                double minDist = measurer(defects[iDefect], centers[0]);
                for(quint64 iClass = 1; iClass < nClasses_; ++iClass)
                {
                    double dist = measurer(defects[iDefect], centers[iClass]);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        iNearestClass = iClass;
                    }
                }
                defectClass[iDefect] = iNearestClass;
                newCenters[iNearestClass] += defects[iDefect];
                ++classSize[iNearestClass];
            }
            isEnd = true;
            for(quint64 iClass = 0; iClass < nClasses_; ++iClass)
            {
                newCenters[iClass] /= classSize[iClass];
                if (isEnd)
                {
                    if (measurer(newCenters[iClass], centers[iClass]) > EPS)
                    {
                        isEnd = false;
                    }
                }
            }
            centers = std::move(newCenters);
            newCenters.resize(nClasses_);
        }
    }

    QVector<RBoundaryTemp> temps;
    temps.reserve(defects.size());
    {
        QFile tempF(tempFile);
        if (!tempF.open(QFile::Text | QFile::ReadOnly))
        {
            return false;
        }
        QTextStream tempStream(&tempF);
        while (!tempStream.atEnd())
        {
            RBoundaryTemp t;
            tempStream >> t;
            temps.append(t);
        }
    }
    if (temps.size() != defects.size())
    {
        return false;
    }

    QJsonObject jsonMainObj;
    QJsonArray jsonClasses;
    QVector<RBoundaryTemp> classMediumTemps(centers.size(), RBoundaryTemp(temps.first().size(), 0));

//    ЗАКОМЕНТИРОВАННЫЙ КОД - запись членов класса

//    QVector<QJsonArray> jsonClassMembers(centers.size());
    for(quint64 iDefect = 0; iDefect < defects.size(); ++iDefect)
    {
        quint64 iClass = defectClass[iDefect];
        const auto& temp = temps[iDefect];
        classMediumTemps[iClass] += temp;
//        QJsonObject jsonMember;
//        jsonMember[JSON_DEFECT] = defects[iDefect].createJsonObject();
//        jsonMember[JSON_BOUNDARY_TEMP] = temp.createJsonArray();
//        jsonClassMembers[iClass].append(jsonMember);
    }
    for(quint64 iClass = 0; iClass < nClasses_; ++iClass)
    {
        QJsonObject jsonClass;
        jsonClass[JSON_CENTRE_DEFECT] = centers[iClass].createJsonObject();
        classMediumTemps[iClass] /= classSize[iClass];
        jsonClass[JSON_MEDIUM_BOUNDARY_TEMP] = classMediumTemps[iClass].createJsonArray();
//        jsonClass[JSON_MEMBERS] = jsonClassMembers[iClass];
        jsonClasses.append(jsonClass);
    }
    jsonMainObj[JSON_CLASSES] = jsonClasses;

    QFile outF(outputFile);
    outF.remove();
    if (!outF.open(QFile::WriteOnly))
    {
        return false;
    }

    QJsonDocument jsonDoc(jsonMainObj);
    auto bytes = jsonDoc.toJson();
    if (outF.write(bytes) != bytes.size())
    {
        return false;
    }
    return true;
}

std::optional<std::pair<quint64, Defect> > Classifier::operator()(const RBoundaryTemp &temp,
                                                                  QString file)
{
    QVector<RBoundaryTemp> classMediumTemps;
    QJsonObject jsonMainObj;
    {
        QFile f(file);
        if (!f.open(QFile::ReadOnly))
        {
            return std::nullopt;
        }
        auto jsonDoc = QJsonDocument::fromJson(f.readAll());
        jsonMainObj = jsonDoc.object();
    }
    QJsonArray jsonClasses;
    if (!(jsonMainObj.contains(JSON_CLASSES) && jsonMainObj[JSON_CLASSES].isArray()))
    {
        return std::nullopt;
    }
    jsonClasses = jsonMainObj[JSON_CLASSES].toArray();
    classMediumTemps.reserve(jsonClasses.size());
    for(quint64 iClass = 0; iClass < jsonClasses.size(); ++iClass)
    {
        auto jsonClass = jsonClasses[iClass].toObject();
        if (!(jsonClass.contains(JSON_MEDIUM_BOUNDARY_TEMP) &&
              jsonClass[JSON_MEDIUM_BOUNDARY_TEMP].isArray()))
        {
            return std::nullopt;
        }
        auto jsonTemp = jsonClass[JSON_MEDIUM_BOUNDARY_TEMP].toArray();
        RBoundaryTemp t;
        if (!t.init(jsonTemp))
        {
            return std::nullopt;
        }
        classMediumTemps.append(t);
    }

    quint64 iNearestClass = 0;
    auto minDist = Measurer::calcDist(temp, classMediumTemps[0]);
    if (!minDist)
    {
        return std::nullopt;
    }
    for(quint64 iClass = 1; iClass < classMediumTemps.size(); ++iClass)
    {
        auto dist = Measurer::calcDist(temp, classMediumTemps[iClass]);
        if (!dist)
        {
            return std::nullopt;
        }
        if (dist < minDist)
        {
            minDist = dist;
            iNearestClass = iClass;
        }
    }
    auto jsonClass = jsonClasses[iNearestClass].toObject();
    if (!(jsonClass.contains(JSON_CENTRE_DEFECT) && jsonClass[JSON_CENTRE_DEFECT].isObject()))
    {
        return std::nullopt;
    }
    Defect centreDefect;
    if (!centreDefect.init(jsonClass[JSON_CENTRE_DEFECT].toObject()))
    {
        return std::nullopt;
    }
    return std::pair{iNearestClass, centreDefect};
}

bool Classifier::init(const QJsonObject &obj)
{
    {
        auto v = obj[JSON_N_CLASSES];
        if (!v.isDouble())
        {
            return false;
        }
        setNClasses(v.toInt());
    }
    return true;
}

quint64 Classifier::nClasses() const
{
    return nClasses_;
}

void Classifier::setNClasses(const quint64 &nClasses)
{
    nClasses_ = nClasses;
}
