// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.


#ifndef DEFECT_H
#define DEFECT_H

#include <QTextStream>
#include <QDebug>
#include <QJsonObject>

/**
 * @brief Дефект
 */
class Defect
{
public:
    explicit Defect(double c_, double l_, double a_);
    explicit Defect() = default;

    double c() const;
    void setC(double value);

    double l() const;
    void setL(double value);

    double a() const;
    void setA(double value);

    Defect& operator += (const Defect& defect);
    Defect& operator /= (double value);

    /**
     * @brief Создает JSON объект с информацией о дефекте
     * @return JSON объект
     */
    QJsonObject createJsonObject() const;

    bool init(const QJsonObject &obj);

private:
    double c_; //координата центра
    double l_; //длинна
    double a_; //температурапроводности
};

/**
 * @brief Добавляет информацию о дефекте в отладочный поток
 * @param debug отладочный поток
 * @param image образ
 * @return отладочный поток
 */
QDebug operator <<(QDebug debug, const Defect& image);
/**
 * @brief Добавляет запись о дефекте в csv-формате в текстовый поток
 * @param stream текстовый поток
 * @param image образ
 * @return поток
 * Разделитель между числами : ';'. Информация дефекта записывается в строку, в конце добавляется endl
 */
QTextStream& operator << (QTextStream& stream, const Defect& image);
/**
 * @brief Получает дефекте из текстового потока
 * @param stream текстовый поток
 * @param image образ
 * @return поток
 * Разделитель между числами : ';'. Пологается, что дефекте записан в строку.
 * Если не удается прочесть (нарушение формата), то запись останавливается.
 */
QTextStream& operator >> (QTextStream& stream, Defect& image);

#endif // DEFECT_H
