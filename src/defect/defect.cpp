// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include "defect.h"


constexpr static char SPLITER = ';';
constexpr static const char* JSON_C = "centre";
constexpr static const char* JSON_L = "length";
constexpr static const char* JSON_A = "diffusivity";

QDebug operator <<(QDebug debug, const Defect &defect)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << '(' << defect.c() << "; " << defect.l() << "; "
                    << defect.a() << ')';
    return debug/*.maybeSpace()*/;
}

QTextStream &operator <<(QTextStream &stream, const Defect &defect)
{
    stream << defect.c() << SPLITER << defect.l() << SPLITER << defect.a() << endl;
    return stream;
}

QTextStream &operator >> (QTextStream &stream, Defect &defect)
{
    QString line = stream.readLine();
    QVector<QStringRef> numStrs = line.splitRef(SPLITER, QString::SkipEmptyParts);
    if (numStrs.size() != 3)
    {
        return stream;
    }
    bool isOk;
    defect.setC(numStrs.at(0).toDouble(&isOk));
    defect.setL(numStrs.at(1).toDouble(&isOk));
    defect.setA(numStrs.at(2).toDouble(&isOk));
    if (!isOk)
    {
        return stream;
    }
    return stream;
}

Defect::Defect(double c, double l, double a)
    : c_{c}, l_{l}, a_{a}
{

}

Defect &Defect::operator +=(const Defect &defect)
{
    c_ += defect.c_;
    l_ += defect.l_;
    a_ += defect.a_;
    return *this;
}

Defect &Defect::operator /=(double value)
{
    c_ /= value;
    l_ /= value;
    a_ /= value;
    return *this;
}

QJsonObject Defect::createJsonObject() const
{
    QJsonObject obj;
    obj[JSON_C] = c_;
    obj[JSON_L] = l_;
    obj[JSON_A] = a_;
    return obj;
}

bool Defect::init(const QJsonObject &obj)
{
    {
        auto v = obj[JSON_C];
        if (!v.isDouble())
        {
            return false;
        }
        setC(v.toDouble());
    }
    {
        auto v = obj[JSON_L];
        if (!v.isDouble())
        {
            return false;
        }
        setL(v.toDouble());
    }
    {
        auto v = obj[JSON_A];
        if (!v.isDouble())
        {
            return false;
        }
        setA(v.toDouble());
    }
    return true;
}

double Defect::c() const
{
    return c_;
}

void Defect::setC(double value)
{
    c_ = value;
}

double Defect::l() const
{
    return l_;
}

void Defect::setL(double value)
{
    l_ = value;
}

double Defect::a() const
{
    return a_;
}

void Defect::setA(double value)
{
    a_ = value;
}
