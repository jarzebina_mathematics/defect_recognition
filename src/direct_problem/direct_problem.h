// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DIRECTPROBLEM_H
#define DIRECTPROBLEM_H

/**
 * @brief Стержень
 */
class Rod;

/**
 * @brief Дефект
 */
class Defect;

/**
 * @brief Нагреватель
 */
class IHeater;

class QJsonObject;

/**
 * @brief Прямая задача теплопроводности стежня с дефектом
 * На правом - ГУ 2 рода с нулевым потоком (нет обмена с окр. средой)
 * Обмена с окр.средой - нет. Источников тепла внутри стержня - нет.
 * Дефект - это область с другим коэф.температурапроводности.
 * Агрегирует Нагреватель (в дестркуторе освобождает память)
 */
class DirectProblem
{
public:
    DirectProblem() = delete;

    explicit DirectProblem(IHeater& heater);
    ~DirectProblem();

    double a(double x) const;

    bool init(const QJsonObject &obj);

    double time() const;
    void setTime(double time);

    double initTemp() const;
    void setInitTemp(double initTemp);

    Rod &rod() const;
    void setRod(const Rod &rod);

    Defect &defect() const;
    void setDefect(const Defect &defect);

    const IHeater &heater() const;
    void setHeater(const IHeater &heater);

private:
    double time_ = 7E3; //время (продолжительность эксперимента)
    double initTemp_ = 290; //начальная температура стержня
    Rod& rod_;//стержень
    Defect& defect_; //Дефект
    IHeater& heater_; //Нагреватель

    void initDefectX();

    double defectX1_; //координата начала дефекта
    double defectX2_; //координата конца дефекта

};

#endif // DIRECTPROBLEM_H

