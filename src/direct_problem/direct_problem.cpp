// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include "direct_problem.h"

#include <QJsonObject>

#include "../rod/rod.h"
#include "../defect/defect.h"
#include "../heater/i_heater.h"

constexpr static const char* JSON_T = "time";
constexpr static const char* JSON_INIT_TEMP = "initTemp";
constexpr static const char* JSON_HEATER = "Heater";

DirectProblem::DirectProblem(IHeater &heater)
    : rod_{*new Rod{}}, defect_{*new Defect{}}, heater_{heater}
{
    initDefectX();
}

DirectProblem::~DirectProblem()
{
    delete &heater_;
}

double DirectProblem::a(double x) const
{
    if (x >= defectX1_ && x <= defectX2_)
    {
        return defect().a();
    }
    return rod_.a();
}

bool DirectProblem::init(const QJsonObject &obj)
{
    {
        auto v = obj[JSON_T];
        if (!v.isDouble())
        {
            return false;
        }
        setTime(v.toDouble());
    }
    {
        auto v = obj[JSON_INIT_TEMP];
        if (!v.isDouble())
        {
            return false;
        }
        setInitTemp(v.toDouble());
    }
    {
        auto v = obj[JSON_HEATER];
        if (!v.isObject())
        {
            return false;
        }
        if (!heater_.init(v.toObject()))
        {
            return false;
        }
    }
    return true;
}

double DirectProblem::time() const
{
    return time_;
}

void DirectProblem::setTime(double time)
{
    time_ = time;
}

double DirectProblem::initTemp() const
{
    return initTemp_;
}

void DirectProblem::setInitTemp(double initTemp)
{
    initTemp_ = initTemp;
}

Rod &DirectProblem::rod() const
{
    return rod_;
}

void DirectProblem::setRod(const Rod &rod)
{
    rod_ = rod;
}

Defect &DirectProblem::defect() const
{
    return defect_;
}

void DirectProblem::setDefect(const Defect &defect)
{
    defect_ = defect;
    initDefectX();
}

const IHeater &DirectProblem::heater() const
{
    return heater_;
}

void DirectProblem::setHeater(const IHeater &heater)
{
    heater_ = heater;
}

void DirectProblem::initDefectX()
{
    defectX1_ = defect().c() - defect().l();
    defectX2_ = defect().c() + defect().l();
}
