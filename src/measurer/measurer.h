// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MEASURER_H
#define MEASURER_H

#include <optional>

#include <QVector>

/**
 * @brief Дефект
 */
class Defect;

/**
 * @brief Табличная функция температуры на правой границе во времени
 */
class RBoundaryTemp;

class QJsonObject;

/**
 * @brief Измеритель растояний
 */
class Measurer
{
public:
    /**
     * @brief Вычисляет растояние между дефекиами
     * @param defect1
     * @param defect2
     * @return растояние
     * Метрика Хэмминга с весами
     */
    double operator ()(const Defect& defect1, const Defect& defect2) const;
    /**
     * @brief Вычисляет растояние между табличными функциями температур во времени
     * @param temps1
     * @param temps2
     * @return Растояние
     * @note Размер таблиц должен быть одинаков
     * Метрика Хэмминга
     */
    static std::optional<double> calcDist(const RBoundaryTemp& temp1, const RBoundaryTemp& temp2);

    bool init(const QJsonObject &obj);

    double coefL() const;
    void setCoefL(double coefL);

    double coefA() const;
    void setCoefA(double coefA);

private:
    double coefL_ = 4; //коэффициент перед длинной в метрике (должны выбираться исходя из дипазонов возможных значений параметров дефекта)
    double coefA_ = 6E4; //коэффициент перед температуропроводностью в метрике

};

#endif // MEASURER_H
