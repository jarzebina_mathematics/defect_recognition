// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include "measurer.h"

#include <cmath>

#include "../defect/defect.h"
#include "../r_boundary_temp/r_boundary_temp.h"

constexpr static const char* JSON_L = "coefficientL";
constexpr static const char* JSON_A = "coefficientA";

double Measurer::operator ()(const Defect &defect1, const Defect &defect2) const
{
    return std::abs(defect1.c() - defect2.c()) +
            coefL_ * std::abs(defect1.l() - defect2.l()) +
            coefA_ * std::abs(defect1.a() - defect2.a());
}

std::optional<double> Measurer::calcDist(const RBoundaryTemp &temp1, const RBoundaryTemp &temp2)
{
    if (temp1.size() != temp2.size())
    {
        return {};
    }
    double dist = 0;
    for(quint64 i = 0; i < temp1.size(); ++i)
    {
        dist += std::abs(temp1[i] - temp2[i]);
    }
    return dist;
}

bool Measurer::init(const QJsonObject &obj)
{
    {
        auto v = obj[JSON_L];
        if (!v.isDouble())
        {
            return false;
        }
        setCoefL(v.toDouble());
    }
    {
        auto v = obj[JSON_A];
        if (!v.isDouble())
        {
            return false;
        }
        setCoefA(v.toDouble());
    }
    return true;
}

double Measurer::coefL() const
{
    return coefL_;
}

void Measurer::setCoefL(double coefL)
{
    coefL_ = coefL;
}

double Measurer::coefA() const
{
    return coefA_;
}

void Measurer::setCoefA(double coefA)
{
    coefA_ = coefA;
}
