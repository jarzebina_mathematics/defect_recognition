// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#ifndef R_BOUNDARY_TEMP_H
#define R_BOUNDARY_TEMP_H

#include <QDebug>
#include <QVector>

class QJsonArray;
class QTextStream;

/**
 * @brief Табличная функция температуры на правой границе во времени
 */
class RBoundaryTemp : private QVector<double>
{
public:
    using QVector<double>::QVector;
    using QVector<double>::operator[];
    using QVector<double>::size;
    using QVector<double>::resize;
    using QVector<double>::reserve;
    using QVector<double>::push_back;
    using QVector<double>::begin;
    using QVector<double>::constBegin;
    using QVector<double>::end;
    using QVector<double>::constEnd;

    void append(const RBoundaryTemp &temps);

    RBoundaryTemp& operator += (const RBoundaryTemp& defect);
    RBoundaryTemp& operator /= (double value);

    /**
     * @brief Создает JSON массив
     * @return JSON массив
     */
    QJsonArray createJsonArray() const;

    bool init(const QJsonArray &array);

};

//QDebug operator <<(QDebug debug, const RBoundaryTemp& RBoundaryTemp);
/**
 * @brief Добавляет запись о температуре в csv-формате в текстовый поток
 * @param stream текстовый поток
 * @param RBoundaryTemp Табличная функция температуры
 * @return поток
 * Разделитель между числами : ';'. Табличная функция температуры записывается в строку,
 * при окончании записи - добавляется endl символ
 */
QTextStream& operator << (QTextStream& stream, const RBoundaryTemp& RBoundaryTemp);
/**
 * @brief Получает Табличную функцию температуры из текстового потока
 * @param stream текстовый поток
 * @param RBoundaryTemp образ
 * @return поток
 * Разделитель между числами : ';'. Пологается, что образ записан в строку.
 * Если не удается прочесть (нарушение формата), то чтение прекращается
 */
QTextStream& operator >> (QTextStream& stream, RBoundaryTemp& RBoundaryTemp);

#endif // R_BOUNDARY_TEMP_H
