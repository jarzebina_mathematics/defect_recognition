// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include "r_boundary_temp.h"

#include <utility>

#include <QTextStream>
#include <QJsonArray>

constexpr static char SPLITER = ';';

//QDebug operator <<(QDebug debug, const RBoundaryTemp &rBoundaryTemp)
//{
//    QDebugStateSaver saver(debug);
//    debug.nospace() << '{' << static_cast<QVector<double>>(rBoundaryTemp) << '}';
//    return debug/*.maybeSpace()*/;
//}

QTextStream &operator <<(QTextStream &stream, const RBoundaryTemp &rBoundaryTemp)
{
    for (const auto& temp : rBoundaryTemp)
    {
        stream << temp << SPLITER;
    }
    stream << endl;
    return stream;
}

QTextStream &operator >> (QTextStream &stream, RBoundaryTemp &rBoundaryTemp)
{
    QString line = stream.readLine();
    QVector<QStringRef> strs = line.splitRef(SPLITER, QString::SkipEmptyParts);
    if (!strs.size())
    {
        return stream;
    }
    rBoundaryTemp.resize(strs.size());
    for (quint64 i = 0; i < strs.size(); ++i)
    {
        bool isOk;
        rBoundaryTemp[i] = strs[i].toDouble(&isOk);
        if (!isOk)
        {
            return stream;
        }
    }
    return stream;
}

void RBoundaryTemp::append(const RBoundaryTemp &temps)
{
    QVector<double>::append(temps);
}

RBoundaryTemp &RBoundaryTemp::operator +=(const RBoundaryTemp &defect)
{
    Q_ASSERT(defect.size() == size());
    for (quint64 i = 0; i < size(); ++i)
    {
        this->operator[](i) += defect[i];
    }
    return *this;
}

RBoundaryTemp &RBoundaryTemp::operator /=(double value)
{
    for (auto& temp : *this)
    {
        temp /= value;
    }
    return *this;
}

QJsonArray RBoundaryTemp::createJsonArray() const
{
    QJsonArray array;
    for (const auto& temp : *this)
    {
        array.append(temp);
    }
    return array;
}

bool RBoundaryTemp::init(const QJsonArray &array)
{
    reserve(array.size());
    for (const auto& v : array)
    {
        if (!v.isDouble())
        {
            return false;
        }
        push_back(v.toDouble());
    }
    return true;
}
