// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ROD_H
#define ROD_H

class QJsonObject;

/**
 * @brief Стержень
 */
class Rod
{
public:
    bool init(const QJsonObject &obj);

    double l() const;
    void setL(double l);

    double a() const;
    void setA(double a);

    double lambda() const;
    void setLambda(double lambda);

private:
    double l_ = 1; //длина стержня
    double a_ = 86E-6; //температурапроводности
    double lambda_ = 210; //температурапроводности
};

#endif // ROD_H
