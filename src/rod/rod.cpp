// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include "rod.h"

#include <QJsonObject>

constexpr static const char* JSON_L = "length";
constexpr static const char* JSON_A = "a";
constexpr static const char* JSON_LAMDA = "lambda";

bool Rod::init(const QJsonObject &obj)
{
    {
        auto length = obj[JSON_L];
        if (!length.isDouble())
        {
            return false;
        }
        setL(length.toDouble());
    }
    {
        auto a = obj[JSON_A];
        if (!a.isDouble())
        {
            return false;
        }
        setA(a.toDouble());
    }
    {
        auto v = obj[JSON_LAMDA];
        if (!v.isDouble())
        {
            return false;
        }
        setLambda(v.toDouble());
    }
    return true;
}

double Rod::l() const
{
    return l_;
}

void Rod::setL(double l)
{
    l_ = l;
}

double Rod::a() const
{
    return a_;
}

void Rod::setA(double a)
{
    a_ = a;
}

double Rod::lambda() const
{
    return lambda_;
}

void Rod::setLambda(double lambda)
{
    lambda_ = lambda;
}
