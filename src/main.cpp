// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "direct_problem/direct_problem.h"
#include "direct_problem_solver/direct_problem_solver.h"
#include "classifier/classifier.h"
#include "generator/generator.h"
#include "classifier/classifier.h"
#include "measurer/measurer.h"
#include "heater/contact_heater.h"
#include "heater/heating_flow.h"
#include "rod/rod.h"
#include "r_boundary_temp/r_boundary_temp.h"

int main(int argc, char *argv[])
{
    QTextStream cout(stdout);

    QJsonObject jsonMainObj;
    {
        constexpr const char* OPTIONS_FILE = "settings.json";
        QFile f(OPTIONS_FILE);
        if (!f.open(QFile::ReadOnly))
        {
            cout << QStringLiteral("File %1 not found").arg(OPTIONS_FILE) << endl;;
            return 1;
        }
        auto jsonDoc = QJsonDocument::fromJson(f.readAll());
        jsonMainObj = jsonDoc.object();
        if (jsonMainObj.isEmpty())
        {
            cout << QStringLiteral("JSON is empty") << endl;
            return 1;
        }
    }
    QString defaultPath;
    {
        constexpr const char* KEY = "DefaultPath";
        auto obj = jsonMainObj[KEY];
        if (!obj.isString())
        {
            cout << QStringLiteral("Key %1 not found").arg(KEY) << endl;
            return 1;
        }
        defaultPath = obj.toString();
    }
    Rod rod;
    {
        constexpr const char* KEY = "Rod";
        auto obj = jsonMainObj[KEY];
        if (!obj.isObject())
        {
            cout << QStringLiteral("Key %1 not found").arg(KEY) << endl;
            return 1;
        }
        if (!rod.init(obj.toObject()))
        {
            cout << QStringLiteral("Rod init faul") << endl;
            return 1;
        }
    }
    DirectProblem exam1(*new ContactHeater);
    {
        constexpr const char* KEY = "Exam1";
        auto obj = jsonMainObj[KEY];
        if (!obj.isObject())
        {
            cout << QStringLiteral("Key %1 not found").arg(KEY) << endl;
            return 1;
        }
        if (!exam1.init(obj.toObject()))
        {
            cout << QStringLiteral("Exam1 init faul") << endl;
            return 1;
        }
    }
    DirectProblem exam2(*new PulseHeatingFlow);
    {
        constexpr const char* KEY = "Exam2";
        auto obj = jsonMainObj[KEY];
        if (!obj.isObject())
        {
            cout << QStringLiteral("Key %1 not found").arg(KEY) << endl;
            return 1;
        }
        if (!exam2.init(obj.toObject()))
        {
            cout << QStringLiteral("Exam2 init faul") << endl;
            return 1;
        }
    }
    DirectProblemSolver solver;
    {
        constexpr const char* KEY = "Solver";
        auto obj = jsonMainObj[KEY];
        if (!obj.isObject())
        {
            cout << QStringLiteral("Key %1 not found").arg(KEY) << endl;
            return 1;
        }
        if (!solver.init(obj.toObject()))
        {
            cout << QStringLiteral("Solver init faul") << endl;
            return 1;
        }
    }
    Generator generator;
    {
        constexpr const char* KEY = "Generator";
        auto obj = jsonMainObj[KEY];
        if (!obj.isObject())
        {
            cout << QStringLiteral("Key %1 not found").arg(KEY) << endl;
            return 1;
        }
        if (!generator.init(obj.toObject()))
        {
            cout << QStringLiteral("Generator init faul") << endl;
            return 1;
        }
    }
    Measurer measerer;
    {
        constexpr const char* KEY = "Measurer";
        auto obj = jsonMainObj[KEY];
        if (!obj.isObject())
        {
            cout << QStringLiteral("Key %1 not found").arg(KEY) << endl;
            return 1;
        }
        if (!measerer.init(obj.toObject()))
        {
            cout << QStringLiteral("Measurer init faul") << endl;
            return 1;
        }
    }
    Classifier classifier;
    {
        constexpr const char* KEY = "Classifier";
        auto obj = jsonMainObj[KEY];
        if (!obj.isObject())
        {
            cout << QStringLiteral("Key %1 not found").arg(KEY) << endl;
            return 1;
        }
        if (!classifier.init(obj.toObject()))
        {
            cout << QStringLiteral("classifier init faul") << endl;
            return 1;
        }
    }

    cout << "0) Test direct problem solver (creates csv file with dynamic data)." << endl
         << "1) Generate the initial selection." << endl
         << "2) Classify the initial selection." << endl
         << "3) Test the inverse problem solver." << endl
         << "Selected action: ";
    cout.flush();
    QTextStream cin(stdin);
    int i;
    cin >> i;
    switch (i)
    {
    case 0:
    {
        //ТЕСТ РЕШЕНИЯ ПРЯМОЙ ЗАДАЧИ
        cout << "Enter c = ";
        cout.flush();
        double c;
        cin >> c;
        cout << "Enter l = ";
        cout.flush();
        double l;
        cin >> l;
        cout << "Enter a = ";
        cout.flush();
        double a;
        cin >> a;
        cout << "Enter output file name = ";
        cout.flush();
        QString file;
        cin >> file;
        cout << "Select 1) Exam1 or 2) Exam2 : ";
        cout.flush();
        int i;
        cin >> i;
        DirectProblem* problem;
        switch (i)
        {
        case 1:
        {
            problem = &exam1;
            break;
        }
        case 2:
        {
            problem = &exam2;
            break;
        }
        default:
        {
            cout << "Bad input";
            return 1;
        }
        }
        problem->setDefect(Defect{c, l, a});
        if (!solver(*problem, QString("%1/%2.csv").arg(defaultPath).arg(file)))
        {
            cout << "Solve fault";
            return 1;
        }
        break;
    }
    case 1:
    {
        //ГЕНЕРАЦИЯ НАЧАЛЬНОЙ ВЫБОРКИ
        cout << "Enter output defects file name = ";
        cout.flush();
        QString fileDef;
        cin >> fileDef;
        cout << "Enter output boundary temps file name = ";
        cout.flush();
        QString fileBT;
        cin >> fileBT;
        if (!generator(exam1, exam2, solver, QString("%1/%2.csv").arg(defaultPath).arg(fileDef),
                       QString("%1/%2.csv").arg(defaultPath).arg(fileBT)))
        {
            cout << "Generate fault";
            return 1;
        }
        break;
    }
    case 2:
    {
        //КЛАССИФИКАЦИЯ НАЧАЛЬНОЙ ВЫБОРКИ
        cout << "Enter input defects file name = ";
        cout.flush();
        QString fileDef;
        cin >> fileDef;
        cout << "Enter input boundary temps file name = ";
        cout.flush();
        QString fileBT;
        cin >> fileBT;
        cout << "Enter output classes file name = ";
        cout.flush();
        QString fileC;
        cin >> fileC;
        if (!classifier(QString("%1/%2.csv").arg(defaultPath).arg(fileDef),
                        QString("%1/%2.csv").arg(defaultPath).arg(fileBT),
                        QString("%1/%2.json").arg(defaultPath).arg(fileC), measerer))
        {
            cout << "Create classes faire.";
            return 1;
        }
        break;
    }
    case 3:
    {
        //ОПРЕДЕЛЕНИЕ КЛАССА ДЛЯ "СЛУЧАЙНОГО" СТЕРЖНЯ
        cout << "Enter c = ";
        cout.flush();
        double c;
        cin >> c;
        cout << "Enter l = ";
        cout.flush();
        double l;
        cin >> l;
        cout << "Enter a = ";
        cout.flush();
        double a;
        cin >> a;
        cout << "Enter input classes file name = ";
        cout.flush();
        QString fileC;
        cin >> fileC;
        exam1.setDefect(Defect{c, l, a});
        exam2.setDefect(Defect{c, l, a});
        RBoundaryTemp temp = solver(exam1);
        temp.append(solver(exam2));
        cout << endl << "Boundary temp: " << temp << endl;
        auto result = classifier(temp, QString("%1/%2.json").arg(defaultPath).arg(fileC));
        if (!result)
        {
            cout << "Detect class fault";
            return 1;
        }
        cout << "Class: " << result->first << endl;
        cout << "Defect: " << result->second << endl ;
        break;
    }
    default:
    {
        cout << "Bad input";
        return 1;
    }
    }

    cout << "Ok." << endl;
    cout.flush();
    return 0;
}
