// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#ifndef IHEATER_H
#define IHEATER_H

#include <QJsonObject>

/**
 * @brief Нагреватель
 */
class IHeater
{
public:
    virtual ~IHeater(){}
    virtual quint8 boundaryConditionType() const = 0;
    virtual double operator()(double t) const = 0;
    virtual bool init(const QJsonObject &obj) = 0;
};

#endif // IHEATER_H
