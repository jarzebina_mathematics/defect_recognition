// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#ifndef HEATINGFLOW_H
#define HEATINGFLOW_H

#include "i_heater.h"

/**
 * @brief Импульсный тепловой поток
 */
class PulseHeatingFlow : public IHeater
{
public:
    ~PulseHeatingFlow() override = default;
    quint8 boundaryConditionType() const override;
    double operator()(double t) const override;
    bool init(const QJsonObject &obj) override;

    double getHeatingTime() const;
    void setHeatingTime(double value);

    double getHeatingFlow() const;
    void setHeatingFlow(double value);

private:
    double heatingTime = 1E3; //продолжительность нагревания
    double heatingFlow = 1E5; //величина потока нагревания

};

#endif // HEATINGFLOW_H
