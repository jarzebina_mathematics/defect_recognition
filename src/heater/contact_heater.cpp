// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include "contact_heater.h"

constexpr static const char* JSON_H_T = "heatingTime";
constexpr static const char* JSON_M_T = "maintenancegTime";
constexpr static const char* JSON_C_T = "coolingTime";
constexpr static const char* JSON_TEMP = "targetTemp";
constexpr static const char* JSON_INIT_TEMP = "initTemp";

ContactHeater::ContactHeater()
{
    initCoef();
}

quint8 ContactHeater::boundaryConditionType() const
{
    return 1;
}

double ContactHeater::operator()(double t) const
{
    if (t <= heatingTime_)
    {
        return k1_ * t + initTemp_;
    }
    if (t <= endMaintenance_)
    {
        return targetTemp_;
    }
    if (t <= endCooling_)
    {
        return k2_ * t + b2_;
    }
    return initTemp_;
}

bool ContactHeater::init(const QJsonObject &obj)
{
    double heatingTime;
    double maintenancegTime;
    double coolingTime;
    double targetTemp;
    double initTemp;
    {
        auto v = obj[JSON_H_T];
        if (!v.isDouble())
        {
            return false;
        }
        heatingTime = v.toDouble();
    }
    {
        auto v = obj[JSON_M_T];
        if (!v.isDouble())
        {
            return false;
        }
        maintenancegTime = v.toDouble();
    }
    {
        auto v = obj[JSON_C_T];
        if (!v.isDouble())
        {
            return false;
        }
        coolingTime = v.toDouble();
    }
    {
        auto v = obj[JSON_TEMP];
        if (!v.isDouble())
        {
            return false;
        }
        targetTemp = v.toDouble();
    }
    {
        auto v = obj[JSON_INIT_TEMP];
        if (!v.isDouble())
        {
            return false;
        }
        initTemp = v.toDouble();
    }
    set(heatingTime, maintenancegTime, coolingTime, targetTemp, initTemp);
    return true;
}


void ContactHeater::set(double heatingTime, double maintenancegTime,
                        double coolingTime, double targetTemp, double initTemp)
{
    heatingTime_ = heatingTime;
    maintenanceTime_ = maintenancegTime;
    coolingTime_ = coolingTime;
    targetTemp_ = targetTemp;
    initTemp_ = initTemp;

    initCoef();
}

void ContactHeater::initCoef()
{
    k1_ = (targetTemp_ - initTemp_) / heatingTime_;

    b2_ = (targetTemp_ * endCooling_ - initTemp_ * endMaintenance_) / coolingTime_;
    k2_ = (initTemp_ - targetTemp_) / coolingTime_;

    endMaintenance_ = heatingTime_ + maintenanceTime_;
    endCooling_ = endMaintenance_ + coolingTime_;
}
