// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include "heating_flow.h"

constexpr static const char* JSON_H_T = "heatingTime";
constexpr static const char* JSON_H_F = "heatingFlow";

quint8 PulseHeatingFlow::boundaryConditionType() const
{
    return 2;
}

double PulseHeatingFlow::operator()(double t) const
{
    if (t <= heatingTime)
    {
        return heatingFlow;
    }
    return 0;
}

bool PulseHeatingFlow::init(const QJsonObject &obj)
{
    {
        auto v = obj[JSON_H_T];
        if (!v.isDouble())
        {
            return false;
        }
        setHeatingTime(v.toDouble());
    }
    {
        auto v = obj[JSON_H_F];
        if (!v.isDouble())
        {
            return false;
        }
        setHeatingFlow(v.toDouble());
    }
    return true;
}

double PulseHeatingFlow::getHeatingTime() const
{
    return heatingTime;
}

void PulseHeatingFlow::setHeatingTime(double value)
{
    heatingTime = value;
}

double PulseHeatingFlow::getHeatingFlow() const
{
    return heatingFlow;
}

void PulseHeatingFlow::setHeatingFlow(double value)
{
    heatingFlow = value;
}
