// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CONTACTHEATER_H
#define CONTACTHEATER_H

#include "i_heater.h"

/**
 * @brief Соприкосающийся нагреватель
 * ГУ 1 рода (нагреватель в идеальном контакте со стержнем)
 * Программа нагрева: Линейный нагрев, поддержание, линейное остывание.
 */
class ContactHeater : public IHeater
{
public:
    explicit ContactHeater();
    ~ContactHeater() override = default;
    quint8 boundaryConditionType() const override;
    double operator()(double t) const override;
    bool init(const QJsonObject &obj) override;

    void set(double heatingTime, double maintenancegTime, double coolingTime, double targetTemp,
             double initTemp);
private:
    double heatingTime_ = 1E3; //время выхода на температурный режим
    double maintenanceTime_ = 1E3; //время поддержания температурного режима
    double coolingTime_ = 1E3; //время остывания до исходной температуры
    double targetTemp_ = 500; //температура нагревания
    double initTemp_ = 300; //температура охлаждения (поддерживается после нагревания)

    void initCoef();

    double k1_;
    double b2_;
    double k2_;
    double endMaintenance_;
    double endCooling_;
};

#endif // CONTACTHEATER_H
