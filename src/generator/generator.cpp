// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#include "generator.h"

#include <QFile>

#include "../r_boundary_temp/r_boundary_temp.h"
#include "../direct_problem/direct_problem.h"
#include "../direct_problem_solver/direct_problem_solver.h"
#include "../defect/defect.h"

constexpr static const char* JSON_MIN = "min";
constexpr static const char* JSON_MAX = "max";
constexpr static const char* JSON_STEP = "step";
constexpr static const char* JSON_L = "defectLGrid";
constexpr static const char* JSON_C = "defectCGrid";
constexpr static const char* JSON_A = "defectAGrid";

bool Generator::operator ()(DirectProblem &exam1, DirectProblem &exam2,
                            const DirectProblemSolver &solver,
                            QString defectFile, QString tempFile)
{
    constexpr double EPS = 1E-6;

    QFile defectF(defectFile);
    defectF.remove();
    if (!defectF.open(QFile::Text | QFile::WriteOnly))
    {
        return false;
    }
    QTextStream defectStream(&defectF);
    QFile tempF(tempFile);
    tempF.remove();
    if (!tempF.open(QFile::Text | QFile::WriteOnly))
    {
        return false;
    }
    QTextStream tempStream(&tempF);

    for (double defectC = defectCGrid_.min(); defectC <= defectCGrid_.max() + EPS;
         defectC += defectCGrid_.step())
    {
        for (double defectL = defectLGrid_.min(); defectL <= defectLGrid_.max() + EPS;
             defectL += defectLGrid_.step())
        {
            for (double defectA = defectAGrid_.min(); defectA <= defectAGrid_.max() + EPS;
                 defectA += defectAGrid_.step())
            {
                Defect defect{defectC, defectL, defectA};
                defectStream << defect;
                exam1.setDefect(defect);
                exam2.setDefect(defect);
                auto temps = solver(exam1);
                temps.append(solver(exam2));
                tempStream << temps;
            }
        }
    }
    return true;
}

bool Generator::init(const QJsonObject &obj)
{
    {
        auto v = obj[JSON_C];
        if (!v.isObject())
        {
            return false;
        }
        if (!defectCGrid_.init(v.toObject()))
        {
            return false;
        }
    }
    {
        auto v = obj[JSON_L];
        if (!v.isObject())
        {
            return false;
        }
        if (!defectLGrid_.init(v.toObject()))
        {
            return false;
        }
    }
    {
        auto v = obj[JSON_A];
        if (!v.isObject())
        {
            return false;
        }
        if (!defectAGrid_.init(v.toObject()))
        {
            return false;
        }
    }
    return true;
}

Generator::RangeGrid Generator::defectCGrid() const
{
    return defectCGrid_;
}

void Generator::setDefectCGrid(const RangeGrid &defectCGrid)
{
    defectCGrid_ = defectCGrid;
}

Generator::RangeGrid Generator::defectLGrid() const
{
    return defectLGrid_;
}

void Generator::setDefectLGrid(const RangeGrid &defectLGrid)
{
    defectLGrid_ = defectLGrid;
}

Generator::RangeGrid Generator::defectAGrid() const
{
    return defectAGrid_;
}

void Generator::setDefectAGrid(const RangeGrid &defectAGrid)
{
    defectAGrid_ = defectAGrid;
}

double Generator::RangeGrid::max() const
{
    return max_;
}

void Generator::RangeGrid::setMax(double max)
{
    max_ = max;
}

double Generator::RangeGrid::min() const
{
    return min_;
}

void Generator::RangeGrid::setMin(double min)
{
    min_ = min;
}

double Generator::RangeGrid::step() const
{
    return step_;
}

void Generator::RangeGrid::setStep(double step)
{
    step_ = step;
}

bool Generator::RangeGrid::init(const QJsonObject &obj)
{
    {
        auto v = obj[JSON_MIN];
        if (!v.isDouble())
        {
            return false;
        }
        setMin(v.toDouble());
    }
    {
        auto v = obj[JSON_MAX];
        if (!v.isDouble())
        {
            return false;
        }
        setMax(v.toDouble());
    }
    {
        auto v = obj[JSON_STEP];
        if (!v.isDouble())
        {
            return false;
        }
        setStep(v.toDouble());
    }
    return true;
}
