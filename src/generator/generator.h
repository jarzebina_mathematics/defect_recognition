// Copyright © 2020 D. Korotaev. All rights reserved.

//This file is part of defect_recognition.

//defect_recognition is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//defect_recognition is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with defect_recognition.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GENERATOR_H
#define GENERATOR_H

#include <utility>

#include <QString>

/**
 * @brief Прямая задача теплопроводности одномерного стежня с дефектом
 */
class DirectProblem;

/**
 * @brief Решатель прямой задачи теплопроводности одномерного стежня  с дефектом
 */
class DirectProblemSolver;

class QJsonObject;

/**
 * @brief Генератор обучающей выборки стрежней (прямых задач)
 */
class Generator
{
public:

    class RangeGrid
    {
    public:
        double max() const;
        void setMax(double max);

        double min() const;
        void setMin(double min);

        double step() const;
        void setStep(double step);

        bool init(const QJsonObject &obj);

    private:
        double max_;
        double min_;
        double step_;
    };

    /**
     * @brief Генерирует выборку и сохраняет в csv файлы
     * @param exam1 прямая задача
     * @param exam2 прямая задача
     * @param solver решатель
     * @param defectFile csv файл выборки дефектов
     * @param tempFile csv файл температур на правой границе
     * Для каждого сгенерированного стержня решаются 2 прямые задачи результаты
     * (таблица-функция температур на правой границе) конкатенируются и записываются в файл.
     */
    bool operator ()(DirectProblem &exam1, DirectProblem &exam2, const DirectProblemSolver& solver,
                      QString defectFile, QString tempFile);

    bool init(const QJsonObject &obj);

    RangeGrid defectCGrid() const;
    void setDefectCGrid(const RangeGrid &defectCGrid);

    RangeGrid defectLGrid() const;
    void setDefectLGrid(const RangeGrid &defectLGrid);

    RangeGrid defectAGrid() const;
    void setDefectAGrid(const RangeGrid &defectAGrid);

private:
    RangeGrid defectCGrid_; //сетка диапазона генерирования координаты центра дефекта
    RangeGrid defectLGrid_; //сетка диапазона генерирования длинны дефекта
    RangeGrid defectAGrid_; //сетка диапазона генерирования температурапроводности дефекта

};

#endif // GENERATOR_H
